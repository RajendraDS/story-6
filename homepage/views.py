from django.shortcuts import render
from .models import Aktivitas, Tamu
from .forms import BukuTamuForm, PartisipanForm
from django.http import HttpResponseRedirect
# Create your views here.

def index(request):
    submit = False
    list_aktivitas = Aktivitas.objects.all()
    list_tamu = Tamu.objects.all()
    if request.method == 'POST':
        form = PartisipanForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/?submit=True')
    else:
        form = PartisipanForm()
        if 'submit' in request.GET:
            submit = True
    return render(request, 'index.html', {'form': form, 'submit': submit, 'list_aktivitas': list_aktivitas, 'list_tamu': list_tamu})

def tambah(request):
    submit = False
    if request.method == 'POST':
        form = BukuTamuForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/tambah/?submit=True')
    else:
        form = BukuTamuForm()
        if 'submit' in request.GET:
            submit = True
    return render(request, 'tambah.html', {'form': form, 'submit': submit})
