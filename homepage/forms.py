from django.forms import ModelForm
from .models import Aktivitas, Tamu


class BukuTamuForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Aktivitas
        fields = '__all__'

class PartisipanForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Tamu
        fields = '__all__'

# class ParticipantForm(ModelForm):
#     required_css_class = 'required'

#     class Meta:
#         model = Participant
#         fields = '__all__'
