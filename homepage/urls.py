from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('tambah/', views.tambah, name='tambah'),
    # path('profile/', views.profile, name='profile'),
    # dilanjutkan ...
]