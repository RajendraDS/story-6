from django.contrib import admin

# Register your models here.
from .models import Aktivitas, Tamu
admin.site.register(Aktivitas)
admin.site.register(Tamu)
