from django.db import models
# Create your models here.

class Aktivitas(models.Model):
    kegiatan = models.CharField('Aktivitas', max_length=120, null=True)
    # bisa string kosong
    deskripsi = models.TextField('Deskripsi', blank=True) 
    tanggal = models.DateField('Tanggal', null=True)

    def __str__(self):
        return self.kegiatan


class Tamu(models.Model):
    # menguhubungkan kelas tamu dengan kelas aktivitas sebagai superclassnya 
    kegiatan = models.ForeignKey(Aktivitas, on_delete=models.CASCADE, null=True)
    nama = models.CharField('Name', max_length=120, null=True)

    def __str__(self):
        return self.nama
