from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Aktivitas, Tamu
from .forms import BukuTamuForm, PartisipanForm
from .views import index, tambah
# Create your tests here.

class UnitTestStory6 (TestCase):
    def test_url_aktivitas (self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_func_aktivitas(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_view_redirect_POST_request (self):
        response = self.client.post(
            '', data={'nama': 'rama','kegiatan':'mandi'}
        )

    def test_kelengkapan_wording_html (self):
        response = Client().get('')
        tampilan_html = response.content.decode('utf8')
        self.assertIn("Aktivitas",tampilan_html)
        self.assertIn("Waktu",tampilan_html)
        self.assertIn("Deskripsi",tampilan_html)
        self.assertIn("Partisipan",tampilan_html)

    def test_model_kelas_aktivitas (self):
        aktivitas = Aktivitas.objects.create(
            kegiatan='Unit_testing',
            deskripsi='Unit_testing',
            tanggal='2020-12-22'
        )
        self.assertTrue(isinstance(aktivitas, Aktivitas))
        self.assertEqual('Unit_testing', str(aktivitas))
        self.assertTrue(Client().get('/?submit=True'))

    def test_model_aktivitas_baru (self):
        input_test = Aktivitas (
            kegiatan='Unit_testing',
            deskripsi='Unit_testing',
            tanggal='2020-12-22'
        )
        input_test.save()
        self.assertTrue(isinstance(input_test, Aktivitas))
        self.assertEqual('Unit_testing', str(input_test))

        jumlah = Aktivitas.objects.all().count()
        self.assertEqual(jumlah, 1)


    def test_form_aktivitas_kosong (self):
        form = BukuTamuForm(
            data={'kegiatan': '','deskripsi': '', 'tanggal': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['kegiatan'], ["This field is required."])
        self.assertEqual(form.errors['tanggal'], ["This field is required."])


    def test_form_activity_bukutamu_accepted(self):
        form = BukuTamuForm(
            data={'kegiatan': 'Unit_testing', 'deskripsi': 'Unit_testing', 'tanggal': '2020-12-22'})
        self.assertTrue(form.is_valid())

# -------------------------------------
    def test_url_add_tamu(self):
        response = Client().get('/tambah/')
        self.assertEqual(response.status_code, 200)

    def test_func_tambah_tamu (self) :
        found = resolve ('/tambah/')
        self.assertEqual(found.func,tambah)

    def test_view_redirect_POST_request_2 (self):
        response = self.client.post(
        '/tambah/', data={'kegiatan': '','deskripsi': '', 'tanggal': ''}
    )
    Client().get('/?submit=True')

    def test_model_tamu(self):
        tamu_baru = Tamu.objects.create(nama='Unit_testing')
        self.assertTrue(isinstance(tamu_baru, Tamu))
        self.assertEqual('Unit_testing', str(tamu_baru))

    def test_form_tamu_kosong (self):
        form = PartisipanForm(
            data={'nama': '','kegiatan':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['kegiatan'], ["This field is required."])
        self.assertEqual(form.errors['nama'], ["This field is required."])

    def test_form_tamu_accepted(self):
        aktivitas = Aktivitas.objects.create(
            kegiatan='Unit_testing', deskripsi=' Unit_testing', tanggal='2020-12-22')
        form = PartisipanForm(data={'kegiatan': aktivitas, 'nama': 'ramona'})
        self.assertTrue(form.is_valid())
