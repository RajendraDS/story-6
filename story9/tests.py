from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .apps import Story9Config
import time

# Create your tests here.


class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')


class UnitTesting(TestCase):
    def test_story10_url_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story10_login_url_exist(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_story10_logout_url_exist(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

    def test_create_user(self):
        User.objects.create_user(
            username="rajendra", email="rajendra@gmail.com", password="rajendra123")
        self.assertEqual(User.objects.all().count(), 1)

    def test_post_request(self):
        signup_response = Client().post('/story9/',
                                    {'username': 'rajendra', 'password': 'rajendra123', 'email': 'rajendra@gmail.com'})
        self.assertEqual(signup_response.status_code, 302)
        login_response = Client().post('/story9/login/',
                                    {'username': 'rajendra', 'password': 'rajendra123'})
        self.assertEqual(signup_response.status_code, 302)


